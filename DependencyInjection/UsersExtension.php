<?php

namespace CMS\UsersBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class UsersExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('config.yml');

        $container->setParameter('users.uploads_photo_directory', $config['uploads_photo_directory']);
        $container->setParameter('users.email_templates.registration', $config['email_templates']['registration']);
        $container->setParameter('users.email_templates.forgot_password', $config['email_templates']['forgot_password']);
        $container->setParameter('users.email_templates.confirm_registration', $config['email_templates']['confirm_registration']);

        $container->setParameter('users.email_addresses.for_registration', $config['email_addresses']['for_registration']);
        $container->setParameter('users.email_addresses.for_forgot_password', $config['email_addresses']['for_forgot_password']);
        $container->setParameter('users.email_addresses.for_confirm_registration', $config['email_addresses']['for_confirm_registration']);
    }
}
