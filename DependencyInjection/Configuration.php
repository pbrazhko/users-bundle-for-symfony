<?php

namespace CMS\UsersBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('users');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        $rootNode
            ->children()
                ->arrayNode('email_templates')
                    ->children()
                        ->scalarNode('registration')
                            ->defaultValue('UsersBundle:Users\Mail:registration.html.twig')
                            ->cannotBeEmpty()
                        ->end()
                        ->scalarNode('confirm_registration')
                            ->defaultValue('UsersBundle:Users\Mail:confirm_registration.html.twig')
                            ->cannotBeEmpty()
                        ->end()
                        ->scalarNode('forgot_password')
                            ->defaultValue('UsersBundle:Users\Mail:forgot_password.html.twig')
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('email_addresses')
                    ->children()
                        ->scalarNode('for_registration')
                            ->cannotBeEmpty()
                        ->end()
                        ->scalarNode('for_forgot_password')
                            ->cannotBeEmpty()
                        ->end()
                        ->scalarNode('for_confirm_registration')
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                ->end()
                ->scalarNode('uploads_photo_directory')
                    ->defaultValue('uploads/users')
                    ->cannotBeEmpty()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
