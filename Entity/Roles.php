<?php

namespace CMS\UsersBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * Roles
 */
class Roles implements RoleInterface
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $role;

    /**
     * @var ArrayCollection
     */
    private $children;

    /**
     * @var integer
     */
    private $parent;
    
    /**
     * @var ArrayCollection
     */
    private $users;

    /**
     * @var \DateTime
     */
    private $date_create;

    /**
     * @var \DateTime
     */
    private $date_update;

    /**
     * @var integer
     */
    private $create_by;

    /**
     * @var integer
     */
    private $update_by;

    public function __construct() {
        $this->users = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Roles
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Roles
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set children
     *
     * @param ArrayCollection $children
     * @return Roles
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Add child
     *
     * @param Roles $child
     * @return Roles
     */
    public function addChild($child){
        $this->children->add($child);

        return $this;
    }

    /**
     * Get children
     *
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param int $parent
     * @return $this
     */
    public function setParent($parent){
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return int
     */
    public function getParent(){
        return $this->parent;
    }

    /**
     * Set users
     * 
     * @param ArrayCollection $users
     * @return CMS\UsersBundle\Entity\Roles
     */
    public function setUsers($users){
        $this->users = $users;
        
        return $this;
    }
    
    /**
     * Get users
     * 
     * @return ArrayCollection
     */
    public function getUsers(){
        return $this->users;
    }
    
    /**
     * Set date_create
     *
     * @param \DateTime $dateCreate
     * @return Roles
     */
    public function setDateCreate($dateCreate)
    {
        $this->date_create = new \DateTime();

        return $this;
    }

    /**
     * Get date_create
     *
     * @return \DateTime 
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * Set date_update
     *
     * @param \DateTime $dateUpdate
     * @return Roles
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->date_update = new \DateTime();

        return $this;
    }

    /**
     * Get date_update
     *
     * @return \DateTime 
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * Set create_by
     *
     * @param integer $createBy
     * @return Roles
     */
    public function setCreateBy($createBy)
    {
        $this->create_by = $createBy;

        return $this;
    }

    /**
     * Get create_by
     *
     * @return integer 
     */
    public function getCreateBy()
    {
        return $this->create_by;
    }

    /**
     * Set update_by
     *
     * @param integer $updateBy
     * @return Roles
     */
    public function setUpdateBy($updateBy)
    {
        $this->update_by = $updateBy;

        return $this;
    }

    /**
     * Get update_by
     *
     * @return integer 
     */
    public function getUpdateBy()
    {
        return $this->update_by;
    }
}
