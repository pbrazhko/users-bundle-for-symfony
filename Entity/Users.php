<?php

namespace CMS\UsersBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Users
 */
class Users implements UserInterface, \Serializable, AdvancedUserInterface
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $last_name;

    /**
     * @var string
     */
    private $middle_name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var integer
     */
    private $photo;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string;
     */
    private $hash;

    /**
     * @var string
     */
    private $salt;
    
    /**
     * @var ArrayCollection
     */
    private $roles;

    /**
     * @var boolean
     */
    private $is_deleted = false;

    /**
     * @var boolean
     */
    private $is_active = true;

    /**
     * @var boolean
     */
    private $is_confirm = false;

    /**
     * @var string
     */
    private $resource = 'self';

    /**
     * @var \DateTime
     */
    private $date_create;

    /**
     * @var \DateTime
     */
    private $date_update;

    /**
     * @var integer
     */
    private $create_by;

    /**
     * @var integer
     */
    private $update_by;

    /**
     *
     */
    public function __construct() {
        $this->roles = new ArrayCollection();
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Users
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     * @return Users
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set middle_name
     *
     * @param string $middleName
     * @return Users
     */
    public function setMiddleName($middleName)
    {
        $this->middle_name = $middleName;

        return $this;
    }

    /**
     * Get middle_name
     *
     * @return string 
     */
    public function getMiddleName()
    {
        return $this->middle_name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Users
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

         return $this;
    }

    /**
     * @return int
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param int $photo
     * @return $this
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

         return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Users
     */
    public function setPassword($password)
    {
        $_password = trim($password);
        if (!empty($_password)){
            $this->password = $password;
        }

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set hash
     */
    public function setHash()
    {
        $this->hash = md5($this->getEmail() . $this->getPassword() . $this->getSalt());
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }
    
    /**
     * Set roles
     * 
     * @param ArrayCollection $roles
     * @return Users
     */
    public function setRoles($roles){
        $this->roles = $roles;
        
        return $this;
    }
    
    /**
     * Get roles
     * 
     * @return array
     */
    public function getRoles(){
        return $this->roles->toArray();
    }

    /**
     * Set is_deleted
     *
     * @param boolean $isDeleted
     * @return Users
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;

        return $this;
    }

    /**
     * Get is_deleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return Users
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Set is_confirm
     *
     * @param boolean $isConfirm
     * @return Users
     */
    public function setIsConfirm($isConfirm)
    {
        $this->is_confirm = $isConfirm;

        return $this;
    }

    /**
     * Get is_confirm
     *
     * @return boolean 
     */
    public function getIsConfirm()
    {
        return $this->is_confirm;
    }

    /**
     * @return string
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @param string $resource
     * @return $this
     */
    public function setResource($resource)
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * Set date_create
     *
     * @param \DateTime $dateCreate
     * @return Users
     */
    public function setDateCreate($dateCreate = null)
    {
        $this->date_create = new \DateTime();

        return $this;
    }

    /**
     * Get date_create
     *
     * @return \DateTime 
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * Set date_update
     *
     * @param \DateTime $dateUpdate
     * @return Users
     */
    public function setDateUpdate($dateUpdate = null)
    {
        $this->date_update = new \DateTime();

        return $this;
    }

    /**
     * Get date_update
     *
     * @return \DateTime 
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }

    /**
     * Set create_by
     *
     * @param integer $createBy
     * @return Users
     */
    public function setCreateBy($createBy)
    {
        $this->create_by = $createBy;

        return $this;
    }

    /**
     * Get create_by
     *
     * @return integer 
     */
    public function getCreateBy()
    {
        return $this->create_by;
    }

    /**
     * Set update_by
     *
     * @param integer $updateBy
     * @return Users
     */
    public function setUpdateBy($updateBy)
    {
        $this->update_by = $updateBy;

        return $this;
    }

    /**
     * @return bool
     */
    public function eraseCredentials() {
        return true;
    }

    /**
     * @return string
     */
    public function getSalt() {
        return $this->salt;
    }

    /**
     * @param string $salt
     * @return $this
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername() {
        return $this->last_name . " " . $this->name;
    }

    /**
     * @return bool
     */
    public function isAccountNonExpired() {
        return true;
    }

    /**
     * @return bool
     */
    public function isAccountNonLocked() {
        return $this->is_active;
    }

    /**
     * @return bool
     */
    public function isCredentialsNonExpired() {
        return true;
    }

    /**
     * @return bool
     */
    public function isEnabled() {
        return $this->is_deleted?false:true;
    }

    /**
     * @return string
     */
    public function serialize() {
        return serialize(array(
            $this->id,
            $this->name,
            $this->last_name,
            $this->email
        ));
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized) {
        list($this->id, $this->name, $this->last_name, $this->email) = unserialize($serialized);
    }

}
