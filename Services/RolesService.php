<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 08.10.14
 * Time: 15:00
 */

namespace CMS\UsersBundle\Services;


use CMS\CoreBundle\AbstractCoreService;
use CMS\UsersBundle\Form\RolesType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class RolesService extends AbstractCoreService
{

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'UsersBundle:Roles';
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            RolesType::class,
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }
}