<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 20.04.15
 * Time: 11:48
 */

namespace CMS\UsersBundle\Services;


use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

class PasswordEncoderSHA512 implements PasswordEncoderInterface
{
    /**
     * Encodes the raw password.
     *
     * @param string $raw The password to encode
     * @param string $salt The salt
     *
     * @return string The encoded password
     */
    public function encodePassword($raw, $salt)
    {
        return hash('sha512', $salt . $raw);
    }

    /**
     * Checks a raw password against an encoded password.
     *
     * @param string $encoded An encoded password
     * @param string $raw A raw password
     * @param string $salt The salt
     *
     * @return bool true if the password is valid, false otherwise
     */
    public function isPasswordValid($encoded, $raw, $salt)
    {
        return $encoded === $this->encodePassword($raw, $salt);
    }

}