<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 30.09.14
 * Time: 13:10
 */

namespace CMS\UsersBundle\Services;

use CMS\CoreBundle\AbstractCoreService;
use CMS\UsersBundle\Entity\Users;
use CMS\UsersBundle\Exceptions\UserNotFoundException;
use CMS\UsersBundle\Form\UsersType;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Util\SecureRandom;
use Symfony\Component\Validator\Constraints\Email;

class UsersService extends AbstractCoreService
{

    /**
     * @param UserResponseInterface $response
     * @return Users
     */
    public function createUser(UserResponseInterface $response){

        $rolesService = $this->container->get('cms.roles.service');

        $user = new Users();

        $user->setEmail($response->getEmail());
        $user->setRoles($rolesService->findByRole('ROLE_USER'));
        $user->setResource($response->getResourceOwner()->getName());

        $passwordGenerator = new SecureRandom();
        $password = $passwordGenerator->nextBytes(10);

        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $user->setPassword($encoder->encodePassword($password, $user->getSalt()));

        $this->create($user);

        $this->sendMailNewUser($user, $password);

        return $user;
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'UsersBundle:Users';
    }

    /**
     * @param UploadedFile $image
     * @return string
     */
    public function uploadPhoto(UploadedFile $image)
    {
        $filename = sha1(uniqid(mt_rand(), true));
        $path = $filename . '.' . $image->guessExtension();

        $image->move($this->getUploadImageRootDir(), $path);

        return $path;
    }

    /**
     * @param $email
     * @return Users
     * @throws UserNotFoundException
     */
    public function forgotPass($email)
    {
        if (empty($email)) {
            throw new \InvalidArgumentException(sprintf('Email is required!'));
        }

        $validator = $this->container->get('validator');
        $emailConstraint = new Email();

        if (!$validator->validate($email, $emailConstraint)) {
            throw new \InvalidArgumentException(sprintf('Email \"%s"\ is not valid!', $email));
        }

        /** @var Users $user */
        $user = $this->findOneBy(['email' => $email]);

        if (!$user) {
            throw new UserNotFoundException(sprintf('User (%s) not found!', $email));
        }

        $password = $this->generatePassword();

        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $user->setPassword($encoder->encodePassword($password, $user->getSalt()));

        $this->update($user);

        $this->sendMailForgotPassword($user, $password);

        return $user;
    }

    /**
     * @param int $length
     * @return string|void
     */
    public function generatePassword($length = 10){
        return random_bytes($length);
    }

    /**
     * @return string
     */
    private function getUploadImageRootDir()
    {
        return __DIR__ . '/../../../../../../web/' . $this->getUploadImageDir();
    }

    /**
     * @return mixed
     */
    private function getUploadImageDir()
    {
        return $this->container->getParameter('users.uploads_photo_directory');
    }

    public function sendMailNewUser(Users $user, $password, $confirmRegistrationRouteName)
    {
        $message = \Swift_Message::newInstance();

        $message
            ->setSubject($this->container->get('translator')->trans('labels.mail.registration.subject', [], 'users'))
            ->setFrom($this->container->getParameter('users.email_addresses.for_registration'))
            ->setTo($user->getEmail())
            ->setBody(
                $this->container->get('templating')->render($this->container->getParameter('users.email_templates.registration'), [
                    'user' => $user,
                    'password' => $password,
                    'confirm_registration_route_name' => $confirmRegistrationRouteName
                ]),
                'text/html'
            );

        return $this->container->get('mailer')->send($message);
    }

    public function sendMailConfirmUser(Users $user)
    {
        $message = \Swift_Message::newInstance();

        $message
            ->setSubject($this->container->get('translator')->trans('labels.mail.confirm_registration.subject', [], 'users'))
            ->setFrom($this->container->getParameter('users.email_addresses.for_confirm_registration'))
            ->setTo($user->getEmail())
            ->setBody(
                $this->container->get('templating')->render($this->container->getParameter('users.email_templates.confirm_registration'), [
                    'user' => $user
                ]),
                'text/html'
            );

        return $this->container->get('mailer')->send($message);
    }

    public function sendMailForgotPassword(Users $user, $password)
    {
        $message = \Swift_Message::newInstance();

        $message
            ->setSubject($this->container->get('translator')->trans('labels.mail.forgot_password.subject', [], 'users'))
            ->setFrom($this->container->getParameter('users.email_addresses.for_forgot_password'))
            ->setTo($user->getEmail())
            ->setBody(
                $this->container->get('templating')->render($this->container->getParameter('users.email_templates.forgot_password'), [
                    'user' => $user,
                    'password' => $password
                ]),
                'text/html'
            );

        return $this->container->get('mailer')->send($message);
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            UsersType::class,
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array(
            'is_deleted' => false
        );
    }
}