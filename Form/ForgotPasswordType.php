<?php
/**
 * Created by PhpStorm.
 * User: work-pc
 * Date: 08.05.16
 * Time: 21:16
 */

namespace CMS\UsersBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ForgotPasswordType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email');
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_usersbundle_forgot_password';
    }
}