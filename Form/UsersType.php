<?php

namespace CMS\UsersBundle\Form;

use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use EWZ\Bundle\RecaptchaBundle\Form\Type\RecaptchaType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class UsersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('last_name')
            ->add('middle_name')
            ->add('email', EmailType::class, array(
                'label' => 'E-mail',
                'required' => true
            ))
            ->add('phone', TextType::class, array(
                'required' => false
            ))
            ->add('roles', EntityType::class, array(
                'class' => 'UsersBundle:Roles',
                'choice_label' => 'name',
                'multiple' => true
            ))
            ->add('photo', UsersPhotoType::class)
            ->add('attach', FileType::class, array(
                'required' => false,
                'mapped' => false,
                'label' => 'Photo',
                'constraints' => array(
                    new Image(array(
                        'minWidth' => 130,
                        'minWidthMessage' => "Min. width size of photo 130",
                        'maxWidth' => 256,
                        'maxWidthMessage' => "Max. width size of photo 150px",
                        'minHeight' => 130,
                        'minHeightMessage' => "Min. height size of photo 130px",
                        'maxHeight' => 256,
                        'maxHeightMessage' => "Max. height size of photo 150px"
                    ))
                )
            ))
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'required' => false,
                'first_options'  => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password'),
            ))
            ->add('recaptcha', EWZRecaptchaType::class, array(
                'attr' => array(
                    'options' => array(
                        'theme' => 'light',
                        'type'  => 'image',
                        'size'  => 'normal',
                        'defer' => true,
                        'async' => true
                    )
                ),
                'mapped' => false,
                'constraints' => array(
                    new IsTrue()
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\UsersBundle\Entity\Users',
            'translation_domain' => 'systems'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_usersbundle_users';
    }
}
