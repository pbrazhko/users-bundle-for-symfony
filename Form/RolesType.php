<?php

namespace CMS\UsersBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RolesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('role')
            ->add('parent', EntityType::class, array(
                'class' => 'UsersBundle:Roles',
                'choice_label' => 'name',
                'mapped' => false,
                'empty_data' => '',
                'placeholder' => '',
                'required' => false
            ))
            ->add('users', EntityType::class, array(
                'class' => 'UsersBundle:Users',
                'choice_label' => 'email',
                'multiple' => true,
                'empty_data' => '',
                'placeholder' => '',
                'required' => false
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\UsersBundle\Entity\Roles',
            'translation_domain' => 'systems'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_usersbundle_roles';
    }
}
