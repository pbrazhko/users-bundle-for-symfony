<?php

namespace CMS\UsersBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;

class ParametersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                $builder->create('main', FormType::class, array('by_reference' => false))
                    ->add('default_user_role', EntityType::class, array(
                        'class' => 'UsersBundle:Roles',
                        'property' => 'name'
                    ))
            )
            ->add(
                $builder->create('registration', FormType::class, array('by_reference' => false))
                    ->add('enabled', CheckboxType::class, array('required' => false))
                    ->add('confirm', CheckboxType::class, array('required' => false))
                    ->add('send_email', CheckboxType::class, array('required' => false))
            );
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_usersbundle_parameters';
    }
}
