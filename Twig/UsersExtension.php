<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 05.08.14
 * Time: 14:11
 */

namespace CMS\UsersBundle\Twig;

use CMS\UsersBundle\Entity\Roles;
use CMS\UsersBundle\Exceptions\UserNotFoundException;
use CMS\UsersBundle\Form\ForgotPasswordType;
use IteratorAggregate;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Util\SecureRandom;

class UsersExtension extends \Twig_Extension{

    private $env;
    private $container;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
    }

    public function getFunctions(){
        return array(
            new \Twig_SimpleFunction('cms_user_profile', [$this, 'userProfile'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('cms_users_show_photo', [$this, 'showPhoto'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('cms_show_login_form', [$this, 'showLoginForm'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('cms_show_registration_form', [$this, 'showRegistrationForm'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('cms_show_forgot_password_form', [$this, 'showForgotPasswordForm'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('cms_is_granted', [$this, 'isGranted'])
        );
    }

    public function userProfile(\Twig_Environment $environment, $template = 'UsersBundle:Twig:profile.html.twig')
    {
        return $environment->render($template);
    }

    public function showLoginForm(\Twig_Environment $environment, $template = 'UsersBundle:Twig:form_login.html.twig')
    {
        $requestStack = $this->container->get('request_stack');
        $request = $requestStack->getCurrentRequest();
        $session = $request->getSession();

        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(Security::AUTHENTICATION_ERROR);
        }

        return $environment->render($template, array(
            '_username' => $session->get(Security::LAST_USERNAME),
            '_error' => $error
        ));
    }

    public function showRegistrationForm(
        \Twig_Environment $environment,
        $confirmRegistrationRouteName,
        $template = 'UsersBundle:Twig:form_registration.html.twig'
    )
    {
        $success = false;
        $service = $this->container->get('cms.users.service');
        $requestStack = $this->container->get('request_stack');
        $request = $requestStack->getCurrentRequest();

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $rolesService = $this->container->get('cms.roles.service');

                /** @var Users $data */
                $data = $form->getData();

                $data->setRoles($rolesService->findByRole('ROLE_USER'));

                $password = trim($form->get('password')->getData());

                if (empty($password)) {
                    $password = $service->generatePassword();
                }

                $encoder = $this->container->get('security.encoder_factory')->getEncoder($data);
                $data->setPassword($encoder->encodePassword($password, $data->getSalt()));

                $service->create($data);

                $service->sendMailNewUser($data, $password, $confirmRegistrationRouteName);

                $success = true;
            }
        }

        return $environment->render($template, array(
            'form' => $form->createView(),
            'success' => $success
        ));
    }

    public function showForgotPasswordForm(
        \Twig_Environment $environment,
        $template = 'UsersBundle:Twig:form_forgot_password.html.twig'
    )
    {
        $service = $this->container->get('cms.users.service');
        $requestStack = $this->container->get('request_stack');
        $request = $requestStack->getCurrentRequest();
        $formFactory = $this->container->get('form.factory');

        $form = $formFactory->create(ForgotPasswordType::class);

        $parameters = [
            'success' => false
        ];

        if($request->isMethod('POST')){
            $form->handleRequest($request);

            if($form->isValid()){
                try {
                    $parameters = [
                        'user' => $service->forgotPass($form->get('email')->getData()),
                        'success' => true
                    ];
                }
                catch(\InvalidArgumentException $e){
                    $form->get('email')->addError(new FormError($e->getMessage()));
                }
                catch(UserNotFoundException $e){
                    $form->get('email')->addError(new FormError($e->getMessage()));
                }
            }
        }

        $parameters['form'] = $form->createView();

        return $environment->render($template, $parameters);
    }

    public function isGranted($attributes)
    {
        $context = $this->container->get('security.authorization_checker');

        $result = false;

        if ($attributes instanceof IteratorAggregate){
            foreach($attributes as $attribute){
                if ($attribute instanceof Roles){
                    $attribute = $attribute->getRole();
                }

                if (true === $context->isGranted($attribute)){
                    $result = true;
                    break;
                }
            }
        }
        elseif (is_array($attributes)){
            foreach($attributes as $attribute){
                if (true === $context->isGranted($attribute)){
                    $result = true;
                    break;
                }
            }
        }
        elseif (is_string($attributes)){
            $result = $context->isGranted($attributes);
        }

        return $result;
    }

    public function showPhoto($path)
    {
        return $this->container->getParameter('users.uploads_photo_directory') . '/' . $path;
    }

    public function getName()
    {
        return 'users_extension';
    }
} 