<?php
namespace CMS\UsersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of UsersCommand
 *
 * @author Brazhko Pavel <cofs2006@gmail.com>
 */
class ChangePasswordCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName("cms:change-user-password")
            ->setDescription("Change password for user")
            ->addOption('email', null, InputOption::VALUE_REQUIRED, 'User E-mail address')
            ->addOption('password', null, InputOption::VALUE_REQUIRED, 'Password for user');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $email = $input->getOption('email');

        if (!preg_match("/^[a-zA-Z0-9\.\-\_]+\@[a-zA-Z0-9\.\-\_]+\.[a-zA-Z]{2,4}$/i", $email)) {
            throw new \RuntimeException('Email is not valid!');
        }

        $password = $input->getOption('password');
        if (mb_strlen($password) < 3) {
            throw new \RuntimeException('The password does not meet the requirements of');
        }

        $user = $em->getRepository('UsersBundle:Users')->findOneByEmail($email);

        if (!$user) {
            throw new \RuntimeException('User not found');
        }

        $factory = $this->getContainer()->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);

        $password = $encoder->encodePassword($password, $user->getSalt());

        $user->setPassword($password);

        $em->persist($user);
        $em->flush();

        $output->writeln('<info>User password is changed!</info>');
    }

}