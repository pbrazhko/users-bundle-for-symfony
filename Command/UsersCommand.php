<?php
namespace CMS\UsersBundle\Command;

use CMS\UsersBundle\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of UsersCommand
 *
 * @author Brazhko Pavel <cofs2006@gmail.com>
 */
class UsersCommand extends ContainerAwareCommand {

    protected function configure() {
        $this
                ->setName("cms:create-user")
                ->setDescription("Create new user")
                ->addOption('email', null, InputOption::VALUE_REQUIRED, 'User E-mail address')
                ->addOption('password', null, InputOption::VALUE_REQUIRED, 'Password for user')
                ->addOption('roles', null, InputOption::VALUE_REQUIRED|InputOption::VALUE_IS_ARRAY, 'Roles for user');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $email = $input->getOption('email');
        
        if (!preg_match("/^[a-zA-Z0-9\.\-\_]+\@[a-zA-Z0-9\.\-\_]+\.[a-zA-Z]{2,4}$/i", $email)){
            throw new \RuntimeException('Email is not valid!');
        }

        $password = $input->getOption('password');
        if (mb_strlen($password) < 3){
            throw new \RuntimeException('The password does not meet the requirements of');
        }

        $roles = $input->getOption('roles');
        if (count($roles) < 1){
            throw new \RuntimeException('The roles does not meet the requirements of');
        }
        
        $_roles = $em->getRepository('UsersBundle:Roles')->findByRole($roles);
        
        if (count($_roles) < 1){
            throw new \RuntimeException('The roles does not exists! Before create roles.');
        }

        $newUser = new Users();

        $factory = $this->getContainer()->get('security.encoder_factory');
        $encoder = $factory->getEncoder($newUser);

        $password = $encoder->encodePassword($password, $newUser->getSalt());

        $newUser->setEmail($email)
                ->setPassword($password)
                ->setIsActive(true)
                ->setIsDeleted(false)
                ->setRoles($_roles)
                ->setIsConfirm(true);

        $em->persist($newUser);
        $em->flush();

        $output->writeln('<info>The user added!</info>');
    }

}