<?php
namespace CMS\UsersBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use CMS\UsersBundle\Entity\Roles;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;

/**
 * Description of RolesCommand
 *
 * @author Brazhko Pavel <cofs2006@gmail.com>
 */
class RolesCommand extends ContainerAwareCommand {

    protected function configure() {
        $this
                ->setName("cms:create-role")
                ->setDescription("Create role for users")
                ->addOption('name', null, InputOption::VALUE_REQUIRED, 'Name role')
                ->addOption('role', null, InputOption::VALUE_REQUIRED, 'Role')
                ->addOption('parent', null, InputOption::VALUE_OPTIONAL, 'Parent role');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $name = $input->getOption('name');
        
        if (empty($name)){
            throw new \RuntimeException('Name is not valid!');
        }

        $role = $input->getOption('role');
        if (!preg_match('/^ROLE\_[A-Z0-9\_]+$/', $role)){
            throw new \RuntimeException('Role is not correct. Example ROLE_USER');
        }

        $newRole = new Roles();
        
        $newRole->setName($name)
                ->setRole($role);

        $em->persist($newRole);

        if ($input->hasOption('parent')){
            $parent = $input->getOption('parent');

            if (!empty($parent) && (is_int($parent) || is_integer($parent))){
                $_role = $em->getRepository('UsersBundle:Roles')->findOneBy(array('id' => $parent));

                if (count($_role) < 1){
                    throw new \RuntimeException('The roles does not exists! Before create roles.');
                }

                $_role->addChild($newRole);
            }
        }

        $em->flush();


        $output->writeln('<info>The role added!</info>');
    }

}