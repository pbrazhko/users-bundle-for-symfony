(
    function($){
        $.UsersUIParameters = function (){
            return $.UsersUIParameters.impl.init();
        };
        
        $.UsersUIParameters.save = function (){
            return $.UsersUIParameters.impl.save();
        };
        
        $.UsersUIParameters.impl = {
            controllElements: {},
            value: null,
            init: function (){
                this.controllElements = this.findControllElements();
                this.bind();
            },
            
            findControllElements: function (){
                return $.find('[data-action]');
            },
            
            bind: function (){
                if (this.controllElements.length){
                    for(var i = 0; i <= this.controllElements.length; i++){
                        var element = this.controllElements[i];
                        var event = null, callback = null;
                        
                        switch($(element).attr('data-action')){
                            case 'edit':
                                event = 'dblclick';
                                callback = this.edit;
                                break;
                        }
                        $(element).bind(event, callback);
                    }
                }
            },
            
            edit: function(){
                this.value = $(this).text().trim();
                
                var input = $('<input/>').attr({'type': 'text', 'value': this.value}).focusout($.UsersUIParameters.save);
                
                $(this).html(input);
            },
            
            save: function (){
                var newValue = $(event.target).val();
            }
        };
    }
)(jQuery);