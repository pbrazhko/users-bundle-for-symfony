(
    function($){
        $.UsersUI = function (){
            return $.UsersUI.impl.init();
        };
        
        $.UsersUI.impl = {
            controllElements: {},
            init: function (){
                this.controllElements = this.findControllElements();
                this.bind();
            },
            
            findControllElements: function (){
                return $.find('[data-action]');
            },
            
            bind: function (){
                if (this.controllElements.length){
                    for(var i = 0; i <= this.controllElements.length; i++){
                        var element = this.controllElements[i];
                        var event = null, callback = null;
                        
                        switch($(element).attr('data-action')){
                            case 'changeStatus':
                                event = 'click';
                                callback = this.changeStatus;
                                break;
                            case 'selectAll':
                                event = 'change';
                                callback = this.selectAll;
                                break;
                            case 'remove':
                                event = 'click';
                                callback = this.remove;
                                break;
                        }
                        $(element).bind(event, callback);
                    }
                }
            },
            
            changeStatus: function(){console.log('change status');},
            selectAll: function(){
                var inputs = $('input[name^="mark"]');
                if (this.checked){
                    inputs.each(function (){this.checked = true;});
                    $('.controllPanel > .buttons > .remove').removeClass('hidden');
                }
                else{
                    inputs.each(function (){this.checked = false;});
                    $('.controllPanel > .buttons > .remove').addClass('hidden');
                }
            },
            remove: function(){console.log('remove');}
        };
    }
)(jQuery);