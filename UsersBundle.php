<?php

namespace CMS\UsersBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class UsersBundle extends Bundle
{
    public function isEnabled(){
        return true;
    }
    
    public function getDescription(){
        return array(
            array(
                'title' => 'Users',
                'defaultRoute' => 'cms_users_list'
            ),
            array(
                'title' => 'Users roles',
                'defaultRoute' => 'cms_roles_list'
            ),
        );
    }
}
