<?php

namespace CMS\UsersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of UsersController
 *
 * @author Brazhko Pavel <cofs2006@gmail.com>
 */
class UsersController extends Controller
{
    
    /**
     * Showing list users for admin interface
     * 
     * @return Response
     */    
    public function listAction(){
        $usersService = $this->get('cms.users.service');
        
        return $this->render('UsersBundle:Users:list.html.twig', array(
            'users' => $usersService->findAll()
        ));
    }

    /**
     * @param $id
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function viewAction($id){
        $usersService = $this->get('cms.users.service');

        $user = $usersService->findOneById($id);
        
        if (!$user){
            throw $this->createNotFoundException('User not found!');
        }
        
        return $this->render('UsersBundle:Users:view.html.twig', array('user' => $user));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function editAction(Request $request, $id){
        $usersService = $this->get('cms.users.service');

        $form = $usersService->generateForm($usersService->findOneById($id));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $password = trim($form->get('password')->getData());

                if (!empty($password)) {
                    $encoder = $this->get('security.encoder_factory')->getEncoder($data);
                    $data->setPassword($encoder->encodePassword($data->getPassword(), $data->getSalt()));
                }

                $photo = $form->get('attach')->getData();
                if($photo && $photo instanceof UploadedFile){
                    $data->setPhoto($usersService->uploadPhoto($photo));
                }

                $data->setUpdateBy($this->getUser()->getId());

                $usersService->update($data);

                return $this->redirect($this->generateUrl('cms_users_view', array('id' => $data->getId())));
            }
        }

        return $this->render('UsersBundle:Users:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id){
        $usersService = $this->get('cms.users.service');

        $usersService->delete($id);

        return $this->redirect($this->generateUrl('cms_users_list'));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeStatusAction($id){

        $usersService = $this->get('cms.users.service');

        $user = $usersService->findOneById($id);

        if (!$user){
            throw $this->createNotFoundException('User not found');
        }

        if ($user->getIsActive()){
            $user->setIsActive(false);
        }
        else{
            $user->setIsActive(true);
        }

        $usersService->update($user);

        return $this->redirect($this->generateUrl('cms_users_list'));
    }
}