<?php

namespace CMS\UsersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CMS\UsersBundle\Form\UsersType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of UsersController
 *
 * @author Brazhko Pavel <cofs2006@gmail.com>
 */
class UsersFController extends Controller{

    /**
     * Show user profile
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return Response
     */
    public function profileAction(){
        $user = $this->getUser();
        
        if (!$user){
            throw $this->createNotFoundException('User not found');
        }
        
        return $this->render('UsersBundle:Users:profile.html.twig', array('user' => $user));
    }

    /**
     * Edit user profile
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return Response
     */
    public function profileEditAction(Request $request){
        $user = $this->getUser();
        
        if (!$user){
            throw $this->createNotFoundException('User not found');
        }
        
        $form = $this->createForm(new UsersType(), $user);
        
        if ($request->isMethod('POST')){
            $form->submit($request);
            
            if ($form->isValid()){
                $data = $form->getData();
                
                $password = $form->get('password')->getData();
                
                if(!empty(trim($password))){
                    $password = $this->encodePassword($password, $data);
                    $data->setPassword($password);
                }
                
                $data->setUpdateBy($this->getUser()->getId());
                
                $this->getManager()->persist($data);
                $this->getManager()->flush();
                
                return $this->redirect($this->generateUrl('users_profile'));
            }
        }
        
        return $this->render('UsersBundle:Users:profileEdit.html.twig',
                array('form' => $form->createView())
        );
    }

    /**
     * Registration new user
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Response
     */
    public function registrationAction(Request $request){
        if (!$this->isRegistrationEnabled()){
            return $this->render('UsersBundle:Users:error.html.twig', array(
                'description' => 'Registration disabled, sorry!'
            ));
        }
        
        $form = $this->createForm(new RegistrationType());
        
        if ($request->isMethod('POST')){
            $form->submit($request);
            
            if ($form->isValid()){
                $data = $form->getData();                            
                
                $password = $this->encodePassword($form->get('password')->getData(), $data);
                
                $data->setPassword($password);
                if ($this->isRegistrationConfirm()){
                    $data->setIsActive(false);
                }

                $this->getManager()->persist($data);
                $this->getManager()->flush();
                
                return $this->redirect($this->generateUrl('users_profile'));
            }
        }
        
        return $this->render('UsersBundle:Users:registration.html.twig',
                array('form' => $form->createView())
        );
    }
}