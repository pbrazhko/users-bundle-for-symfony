<?php

namespace CMS\UsersBundle\Controller;

use CMS\UsersBundle\Form\ParametersType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Dumper;

/**
 * Description of SettingsController
 *
 * @author Brazhko Pavel <cofs2006@gmail.com>
 */
class SettingsController extends Controller
{
    public function listAction(Request $request){
        $parameters = $this->container->getParameter('users');
        
        $form = $this->createForm(new ParametersType(), $parameters);
        
        if ($request->isMethod('POST')){
            $form->submit($request);
            
            if ($form->isValid()){
                $this->saveData(array('users' =>$form->getData()));
                
                return $this->redirect($this->generateUrl('users_admin_list'));
            }
        }
        
        return $this->render('UsersBundle:Settings:prametersList.html.twig', array('form' => $form->createView(), 'parameters' => $parameters));
    }

    private function saveData($data)
    {
        $dumper = new Dumper();

        try {
            $yaml = $dumper->dump(array('parameters' => $data));

            $bundle = $this->get('kernel')->getBundle('UsersBundle');

            file_put_contents(
                $bundle->getPath() . '/Resources/config/config.yml',
                $yaml
            );
        } catch (Exception $ex) {
            return false;
        }

        return true;
    }
}