<?php

namespace CMS\UsersBundle\Controller;

use CMS\UsersBundle\Exceptions\UserNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller {

    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('UsersBundle:Security:login.html.twig', array(
            '_username' => $lastUsername,
            '_error' => $error
        ));
    }
    
    public function checkAction(){
        
    }

    public function logoutAction() {
        return $this->render('UsersBundle:Security:logout.html.twig');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function forgotpassAction(Request $request)
    {
        $error = null;

        if ($request->isMethod('POST')) {
            $usersService = $this->get('cms.users.service');

            try {
                $usersService->forgotPass($request->get('email'));
            } catch (\InvalidArgumentException $error) {
            } catch (UserNotFoundException $error) {
            }
        }

        return $this->render('UsersBundle:Security:forgotpass.html.twig', array(
            'error' => $error
        ));
    }
}