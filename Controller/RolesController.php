<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 29.06.14
 * Time: 19:53
 */

namespace CMS\UsersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RolesController extends Controller
{
    public function listAction(){
        $rolesService = $this->get('cms.roles.service');

        return $this->render('UsersBundle:Roles:list.html.twig', array(
            'roles' => $rolesService->findAll()
        ));
    }

    public function editAction(Request $request, $id){
        $roleService = $this->get('cms.roles.service');

        $form = $roleService->generateForm($roleService->findOneById($id));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $roleService->update($data);

                return $this->redirect($this->generateUrl('cms_roles_list'));
            }
        }

        return $this->render('UsersBundle:Roles:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function addAction(Request $request){
        $service = $this->get('cms.roles.service');

        $form = $service->generateForm();

        if($request->isMethod('POST')){
            $form->handleRequest($request);

            if($form->isValid()){

                $service->create($form->getData());

                return $this->redirect($this->generateUrl('cms_roles_list'));
            }
        }

        return $this->render('UsersBundle:Roles:add.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function deleteAction($id){
        $rolesService = $this->get('cms.roles.service');

        $rolesService->delete($id);

        return $this->redirect($this->generateUrl('cms_roles_list'));
    }
} 