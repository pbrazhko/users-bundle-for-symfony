#Установка:
### Добавляем пакет в AppKernel.php:
    new \CMS\UsersBundle\UsersBundle(),
### Добавляем encoder:
    encodersCMSp\UsersBundle\Entity\Users: sha512
### Устанавливам providers:
    main:
        entity: { class: UsersBundle:Users, property: email }
### Создаем иерархию ролей так как вам это необходимо
В качестве примера:

    role_hierarchy:
        ROLE_ADMIN:       ROLE_USER
        ROLE_SUPER_ADMIN: [ROLE_USER, ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]

### Настраиваем firewall для своего пространства имен
Роут для авторизации (login_path) - users_login
Роут для проверки авторизации (check_path) - users_login_check
Роут для деавторизации (logout.path) - users_logout


